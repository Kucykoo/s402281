const systemsForm = document.getElementById('systemsForm');

const db = require('../config/query');
const { getProjects } = require('../config/query');

const projectUrl = document.getElementById('url');
const projectToken = document.getElementById('token');
const projectID = document.getElementById('projectID');
const projectName = document.getElementById('name');
const gitLab = document.getElementById('gitLab');
const sentry = document.getElementById('sentry');
const projectsList = document.getElementById('projects')

let projects = []; 

const getprojects = async () => {
    projects = await db.getProjects();
    renderprojects(projects);
    // console.log(results);
};

async function deleteProject(id) {
    const response = confirm('Are your sure you want to delete it?')
    if (response) {
        await db.deleteProject(id);
        await getprojects();
    }
    return;
}

function renderprojects(projects) {
    projectsList.innerHTML = '';
    projects.forEach(project => {
        projectsList.innerHTML += `
            <div class="projectContainer">
                <div>${project.name}</div>
                <div>${project.url}</div>
                <div>${project.type}</div>
                <div>
                    <button onclick="deleteProject('${project.id}')">
                        DELETE
                    </button>
                </div>
            </div>
        `;
    });
};

function checkingValuesOfRadio() {
    if (gitLab.checked == true){
        return 'gitLab'
    } else if (sentry.checked == true){
        return 'sentry'
    }
}

systemsForm.addEventListener('submit', (e) => {
    e.preventDefault();

    const newProject = {
        url: projectUrl.value,
        token: projectToken.value,
        projectId: projectID.value,
        name: projectName.value,
        type: checkingValuesOfRadio()
    }
    db.createNewProject(newProject)

    systemsForm.reset();
    projectName.focus();
    getprojects();

}) 

async function init() {
    await getprojects();
};

init();