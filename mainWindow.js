const db = require('../config/query');
let request = require('request');

const projectsList = document.getElementById('projectsMain')
let projects = []; 

const getprojects = async () => {
    projects = await db.getProjects();
    renderProjects(projects);
};

function renderProjects(projects){
    projectsList.innerHTML = '';
    projects.forEach(project => {
        if(project.type == 'gitLab'){
            request(`http://${project.url}/api/v4/projects/${project.projectId}/pipelines?ref=master&per_page=2&page=1&private_token=${project.token}`, function(err, response, body){
            let bodyJson = JSON.parse(body);
            let result = bodyJson[0].status;
            projectsList.innerHTML += `
            <div class="tile${result}">
                <h2>${project.name}</h2>
            </div>
            `
        }
        );
        
        } else 
        if(project.type == 'sentry'){           
            let options = {
                url: `https://${project.url}/api/0/projects/sentry/awsa/issues/`,
                headers: {
                    'Authorization': `Bearer ${project.token}`,
                    'Content-Type': 'application/json'
                }
            };
            request(options, function(err, response, body){
            let bodyJson = JSON.parse(body);
            let issues;
            unresolvedIssues = bodyJson.length
            if(unresolvedIssues == 0 ){
                issues = 'success';
            } else {
                issues = 'pending';
            }
            projectsList.innerHTML += `
            <div class="tile${issues}">
                <h2>${project.name}: ${unresolvedIssues}</h2>
            </div>
            `
            }
            )
        }
    });
};

async function init() {
    await getprojects();
};

init()
    
setInterval(() => {
    init();
},100000);