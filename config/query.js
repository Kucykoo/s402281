
const { getConnection } = require('./config')

async function createNewProject(project){
    try{
        const conn = await getConnection();
        const result = await conn.query('INSERT INTO systems SET ?', project)
    } catch (error) {
        console.log(error)
    }
}
    
async function getProjects() {
    const conn = await getConnection();
    const result = await conn.query('SELECT * FROM systems ORDER BY id DESC');
    // console.log(result);
    return result;
}   

async function deleteProject(id) {
    const conn = await getConnection();
    const result = await conn.query('DELETE FROM systems WHERE id = ?', id)
    return result;
}

module.exports = {
    createNewProject,
    getProjects,
    deleteProject
}