const electron = require('electron');
const url = require('url');
const path = require('path');
require('./config/config')
// require('./config/query')


const {app, BrowserWindow, Menu} = electron;

let mainWindow;
process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true';

//Listen for app to be ready
app.allowRendererProcessReuse = false;
app.on('ready', function(){
    //Create new window
    mainWindow = new BrowserWindow({
        alwaysOnTop: true,
        maximizable: true,
        minWidth: 1200,
        minHeight: 800,
        center: true,
        webPreferences: {
            nodeIntegration: true,
         },
        //  sandbox: true
    });
    //Load html into window
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'views', 'mainWindow.html'),
        protocol: 'file',
        slashes: true
    }));

    //Build menu from templates
    const mainMenu = Menu.buildFromTemplate(mainMenuTemplate)
    // Insert menuset
    Menu.setApplicationMenu(mainMenu)
    mainWindow.webContents.openDevTools()
});

const goToSettings = () => {
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'views', 'settingsWindow.html'),
        protocol: 'file:',
        slashes: true
    }));
} 

const goToMainWindow = () => {
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'views', 'mainWindow.html'),
        protocol: 'file:',
        slashes: true
    }));
} 

// Create menu template

const mainMenuTemplate = [
    {
        label:'Menu',
        submenu:[
            {
                label: 'Go to Main Window',
                click(){
                    goToMainWindow();
                } 
            },
            {
                type: 'separator'
            },
            {
                label: 'Go to Settings',
                click(){
                    goToSettings();
                } 
            },
            {
                type: 'separator'
            },
            {
                label: 'On the Top',
                type: 'checkbox',
                checked: true,
                click: e => {
                    if(e.checked){
                        mainWindow.setAlwaysOnTop(true, 'screen');
                    } else {
                        mainWindow.setAlwaysOnTop(false, 'screen');
                    };
                }
            }
        ]
    }
];

// Add dev tools item if not in prod

if(process.env.NODE_ENV !== 'prod'){
    mainMenuTemplate.push({
        label: 'Dev tools',
        submenu: [
            {
                label: 'Toggle DevTools',
                accelerator: process.platform == 'daerwin' ? 'Command+I' : 'Ctrl+I',
                click(item, focusedWindow) {
                    focusedWindow.toggleDevTools();
                }
            },
            {
                role: 'reload'
            }    
        ]
    })
}
